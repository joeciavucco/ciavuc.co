<?php include './parts/header.php'; ?>

<div class="frontPage flex-v-center flex-center">

    <div class="introduction">
        <div class="text">
            <h2>ciavuc.co</h2>
            <p>I'm Joe Ciavucco, a web developer from Staffordshire, currently working at <a target="_blank" href="https://www.harrisoncarloss.com">Harrison Carloss</a>.</p>
            <p class="email">Email me at <a href="mailto:joe@ciavuc.co">joe@ciavuc.co</a></p>
        </div>
    </div>
    <a class="wolf" href="https://github.com/ciavuc/wolf" target="_blank">🐺</a>
    <a class="do" href="https://m.do.co/c/4bce60f4467b" target="_blank"></a>
</div>

<?php include './parts/footer.php'; ?>
